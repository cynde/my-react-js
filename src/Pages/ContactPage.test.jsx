import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import axios from 'axios';
import { when } from 'jest-when';
import ContactPage from './ContactPage';

jest.mock('axios');

describe('ContactPage', () => {
  beforeEach(() => {
    const response = {
      data: ['Cynde', 'Ranu', 'Monica']
    };
    when(axios.get).calledWith('http://localhost:3004/names').mockResolvedValue(response);
    render(<ContactPage />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render correct contact list', () => {
    const names = screen.getAllByRole('listitem');

    expect(within(names[0]).getByText('Cynde')).toBeInTheDocument();
    expect(within(names[1]).getByText('Ranu')).toBeInTheDocument();
    expect(within(names[2]).getByText('Monica')).toBeInTheDocument();
  });

  it('should add the new contact to the list when submit button is clicked', () => {
    const nameInput = screen.getByRole('textbox', { name: /name/i });
    const submitButton = screen.getByRole('button', { name: /submit/i });
    const newName = 'Davidsen';

    userEvent.type(nameInput, newName);
    userEvent.click(submitButton);
    const names = screen.getAllByRole('listitem');

    expect(within(names[3]).getByText(newName)).toBeInTheDocument();
  });

  it('should show only 1 person when filtered by a name when only 1 name matches', () => {
    const filter = 'Cyn';
    const filterInput = screen.getByRole('textbox', { name: /filter/i });

    userEvent.type(filterInput, filter);
    const filteredName = screen.getByRole('listitem');

    expect(within(filteredName).getByText('Cynde')).toBeInTheDocument();
  });

  it('should show no names when filtered by a name when no names matches', () => {
    const filter = 'Bunga';
    const filterInput = screen.getByRole('textbox', { name: /filter/i });

    userEvent.type(filterInput, filter);

    expect(screen.queryByText('Bunga')).not.toBeInTheDocument();
  });
});
