import PropTypes from 'prop-types';

const ContactFilter = ({ onFilter }) => (
  <label htmlFor="filter">
    Filter:
    <input
      placeholder="Filter"
      id="filter"
      name="filter"
      type="text"
      defaultValue=""
      onChange={onFilter}
    />
  </label>
);

export default ContactFilter;

ContactFilter.propTypes = {
  onFilter: PropTypes.func.isRequired
};
