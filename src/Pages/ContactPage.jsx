import { PureComponent } from 'react';
import axios from 'axios';
import Contact from './Contact';
import ContactForm from './ContactForm';
import ContactFilter from './ContactFilter';

export default class ContactPage extends PureComponent {
  constructor() {
    super();
    this.state = {
      names: []
    };
    this.handleOnClick = this.handleOnClick.bind(this);
    this.handleOnFilter = this.handleOnFilter.bind(this);
  }

  async componentDidMount() {
    const { data } = await axios.get('http://localhost:3004/names');
    this.setState({
      names: data
    });
  }

  handleOnClick(newName) {
    const { names } = this.state;
    this.setState({
      names: [...names, newName]
    });
  }

  handleOnFilter(event) {
    const { value } = event.target;
    const { names } = this.state;
    const filteredNames = names
      .filter((name) => name.toLowerCase().includes(value.toLowerCase()));

    this.setState({
      names: filteredNames
    });
  }

  render() {
    const { names } = this.state;
    return (
      <div>
        <ContactForm onClick={this.handleOnClick} />
        <ContactFilter onFilter={this.handleOnFilter} />
        <ul>
          {names.map((name) => <Contact key={name} name={name} />)}
        </ul>
      </div>
    );
  }
}
