import PropTypes from 'prop-types';

const Contact = ({ name }) => (
  <li key={name}>{name}</li>
);

export default Contact;

Contact.propTypes = {
  name: PropTypes.string.isRequired
};
