import { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class ContactForm extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: ''
    };
    this.handleOnClick = this.handleOnClick.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleOnChange(event) {
    const { value } = event.target;
    this.setState({ name: value });
  }

  handleOnClick() {
    const { onClick } = this.props;
    const { name } = this.state;
    onClick(name);
  }

  render() {
    const { name } = this.state;
    return (
      <div>
        <p>Add New Contact:</p>
        <label htmlFor="name">
          Name:
          <input
            placeholder="Name"
            id="name"
            name="name"
            type="text"
            defaultValue={name}
            onChange={this.handleOnChange}
          />
        </label>
        <button type="submit" onClick={this.handleOnClick}>Submit</button>
      </div>
    );
  }
}

ContactForm.propTypes = {
  onClick: PropTypes.func.isRequired
};
