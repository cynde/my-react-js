import { render, screen } from '@testing-library/react';
import App from './App';

describe('App', () => {
  it('should render hello world', () => {
    render(<App />);
    const linkElement = screen.getByText(/hello world/i);

    expect(linkElement).toBeInTheDocument();
  });
});
