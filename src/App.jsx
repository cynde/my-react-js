import './App.css';
import ContactPage from './Pages/ContactPage';

function App() {
  const names = ['Cynde', 'Ranu', 'Monica'];

  return (
    <div className="App">
      <p>Hello World</p>
      <ContactPage names={names} />
    </div>
  );
}

export default App;
